import os
import json
import redis
import logging
import requests

from flask import Flask, jsonify, request

LOG = logging

app = Flask(__name__)
BASE_URL = 'http://simpleobjectstorage:8080'

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:factoring'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

@app.route('/<bucketname>/', methods=['GET'])
def listGIF(bucketname):
    if  (request.args.get('list') == ''):
        listAll = []
        objects = {}
    
        try:
            res = requests.get(BASE_URL+'/'+bucketname+'?list')

            r = res.content
            if (res.status_code == 200):
                response = json.loads((r).decode('utf8').replace("'", '"'))["objects"]
                for i in range(len(response)):
                    name = response[i].get('name')
                    if (not name.endswith('.gif')):
                        response.remove(i)
                    listAll.append(name)
                objects[bucketname] = listAll            
                return jsonify(objects), 200
            return jsonify({'status': 'BAD REQUEST'}), 400

        except:
            return jsonify({'status': 'BAD REQUEST'}), 400
        
        return jsonify({'status': 'OK'})
    return jsonify({'status': 'BAD REQUEST'}), 400
    
@app.route('/submit', methods=['POST'])
def makeGIF():
    body = request.json
    json_packed = json.dumps(body)
    print('packed:', json_packed)
    info = json.loads(json_packed)
    bucketname = info.get('bucketname')
    objectname = info.get('objectname')
    info["targetbucket"] = "keeper"
    info["targetobject"] = objectname
    
    if (objectname):
        # do only specific object
        RedisResource.conn.rpush(RedisResource.QUEUE_NAME, info)
        return jsonify({'status': 'OK'})
    else:
        # get all objects in bucket
        try:
            r = requests.get(BASE_URL+'/'+bucketname+'?list')
            if (r.status_code == 200):
                response = r.json()
                lst = response["objects"]

                for i in range(len(lst)):
                    info["objectname"] = lst[i]["name"]
                    info["targetobject"] = lst[i]["name"]
                    RedisResource.conn.rpush(RedisResource.QUEUE_NAME, info)
                return jsonify({'status': 'OK'})  
            return jsonify({'status': 'BAD REQUEST'}), 400
            
        except:
            return jsonify({'status': 'BAD REQUEST'}), 400
        

