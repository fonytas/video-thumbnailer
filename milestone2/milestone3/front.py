import requests
import json
import logging

from flask import Flask, render_template, request, redirect
app = Flask(__name__)

SOS = 'http://simpleobjectstorage:8080'
THUMBNAIL = 'http://milestone2_queue-wrapper_1:5000'
GIFMAKER = 'http://milestone2_milestone3_1:3412'
URL = 'http://localhost:3412'

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
@app.route('/')
def homepage():
    
    return render_template('homepage.html', allBucket=getAllBucket())

@app.route('/<bucketname>/show_all_videos')
def showAll(bucketname):

    if (isBucketExist(bucketname)):
        return render_template('showall.html', bucketname=bucketname, videoList=getVideoList(bucketname)) 
    return render_template('error.html', error="bucket not found")

@app.route('/makegif/<bucketname>/<objectname>')
def makeGIF(bucketname, objectname):

    if  (objectname == "all"):
        #do all
        r = requests.post(THUMBNAIL+'/submit', json={"bucketname":bucketname})
        if (r.status_code == 200):
            return render_template('done.html')
        return render_template('error.html', error="fail to make gif")
    else:
        #do only specific object
        r = requests.post(THUMBNAIL+'/submit', json={"bucketname":bucketname, "objectname": objectname}) 
        if (r.status_code == 200):
            return render_template('done.html')
        return render_template('error.html', error="fail to make gif")

@app.route('/<bucketname>/show_gif')
def showGIF(bucketname):
    
    gifList = getAllGIF(bucketname)
    if (len(gifList) > 0):
        return render_template('showgif.html', gifList=gifList, bucketname=bucketname)
    return render_template('error.html', error="no gif in the bucket")

def getAllGIF(bucketname):
    r = requests.get(THUMBNAIL+'/'+bucketname+'?list')
    if (r.status_code == 200):
        
        response = json.loads(r.content)
        lst = response.get(bucketname)
        return lst
    return []

@app.route('/delete/<bucketname>/<objectname>')
def deleteGIF(bucketname, objectname):
    
    if (objectname == "all"):
        
        # delete everything
        r = requests.get(THUMBNAIL+'/'+bucketname+'?list')
        print(r.status_code)
        if (r.status_code == 200):
            response = json.loads(r.content)
            lst = response.get(bucketname)
            for name in lst:
                r = requests.delete(SOS+'/'+bucketname+'/'+name+'?delete')
                if (r.status_code != 200):
                    return redirect(URL+'/'+bucketname+'/show_all_videos')
            return redirect(URL+'/'+bucketname+'/show_all_videos')
    
        return render_template('error.html', error="Delete GIF fail ..")
        
    else:
        #do only specific objectname
        
        r = requests.delete(SOS+'/'+bucketname+'/'+objectname+'?delete')
        if (r.status_code == 200):
            return redirect(URL+'/'+bucketname+'/show_gif')
            
        return render_template('error.html', error="Delete GIF fail ..")
        

def getAllBucket():
    r = requests.get(SOS+'/all')
    if (r.status_code == 200):
        response = json.loads(r.content)
        lst = []
        for i in range(len(response)):
            lst.append(response[i].get('name'))
        return lst
    return []

def isBucketExist(bucketname):
    r = requests.get(SOS+'/'+bucketname+'?list')
    if (r.status_code == 200):
        return True
    return False

def getVideoList(bucketname):
    r = requests.get(SOS+'/'+bucketname+'?list')
    if (r.status_code == 200):
        response = json.loads(r.content)
        objects = response.get('objects')
        lst = []
        for i in range(len(objects)):
            name = objects[i].get('name')
            
            if (name.endswith('.mp4') or name.endswith('.mov') or name.endswith('avi')):
                lst.append(name)
        return lst
    return []




if __name__ == '__main__':
    app.run(debug = True)