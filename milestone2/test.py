import requests
import hashlib

SOS = 'http://localhost:8080'
THUMBNAIL = 'http://localhost:5000'
GIFMAKER = 'http:/localhost:3412'

STATUS_OK = requests.codes['ok']
STATUS_BAD = 400

def test_case():
	resp = requests.get(THUMBNAIL + '/keeper?list')
	assert resp.status_code == STATUS_OK

	resp = requests.get(THUMBNAIL + '/faker?list')
	assert resp.status_code == STATUS_BAD

