#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import requests
import subprocess
import hashlib

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:factoring'
STATUS_OK = requests.codes['ok']

BASE_URL = 'http://simpleobjectstorage:8080'
# URL = 'http://localhost:5000'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
def watch_queue(redis_conn, queue_name, callback_func, timeout=30): #redis
    active = True
    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)
        if not packed:
            # if nothing is returned, poll a again
            continue
        _, packed_task = packed
        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8').replace("'", '"'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)


def downloadAndMakeGif(log,response, bucketname, objectname):

    try:
        if (objectname.endswith('.mp4') or objectname.endswith('.mov') or objectname.endswith('.avi')):
            objectname = objectname[:-4]
            log.info('downloading ..')
            f= open("./data/"+objectname,"wb")
            f.write(response)
            f.close()
            subprocess.run(["./make_thumbnail", objectname, objectname+".gif", INSTANCE_NAME])
            os.remove('./data/'+objectname)
            log.info('Creating gif...done')
        else:
            return jsonify({'status': 'BAD REQUEST'}), 400

    except:
        return jsonify({'status': 'BAD REQUEST'}), 400

def uploadToSOS(log, targetBucket, objectname):
    try:
        log.info('uploading . .')
        objectname = objectname[:-4]
    
        r = requests.post(BASE_URL+'/'+targetBucket+'/'+objectname+'.gif?create')
        if (r.status_code == 200):

            data = open('./data/'+objectname+'.gif', 'rb').read()
            headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
            resp = requests.put(url=BASE_URL+'/'+targetBucket+'/'+objectname+'.gif'+'?partNumber=1',
                                data=data,
                                headers=headers)
            if (resp.status_code == 200):
                resp = requests.post(BASE_URL+'/'+targetBucket+'/'+objectname+'.gif?complete')
                if (resp.status_code == 200):
                    log.info('Uploading gif...done')

    except:
        return jsonify({'status': 'BAD REQUEST'}), 400


def execute_bucket(log, task):
    bucketname = task.get("bucketname")
    objectname = task.get("objectname")
    targetBucket = task.get("targetbucket")
    targetObject = task.get("targetobject")
    try:
        r = requests.get(BASE_URL+'/'+bucketname+'/'+objectname)  
        if (r.status_code == 200):
            response = r.content
            downloadAndMakeGif(log,response, bucketname, objectname)
            uploadToSOS(log, targetBucket, objectname)
    except:
        log.info('FAIL . . . .')

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    os.mkdir('data/'+INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)
    
    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_bucket(named_logging, task_descr))

if __name__ == '__main__':
    main()
